I create this web service in three core layer.

1. Controller - for work with URL, json, conversions from model to json
2. Service - used for processing business logic
3. Dao - storing and retrieving data from DB or some other storing mechanism

Asymptotic behaviour
If, in theory, every transaction have parent_id it can cause memory leak in calculating sum of them. We can limit that to some size or throw some error in case of too big link.

