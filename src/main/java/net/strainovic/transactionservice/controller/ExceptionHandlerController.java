package net.strainovic.transactionservice.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import net.strainovic.transactionservice.model.ResponseMap;

/**
 * Uniforming all exception to not allow client to see some internal data (stack
 * trance)...
 */
@ControllerAdvice
public class ExceptionHandlerController {

    private static final Log LOGGER = LogFactory.getLog(ExceptionHandlerController.class);

    private static final String REASONS = "reasons";

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseMap defaultErrorHandler(Exception e) throws Exception {
	LOGGER.error(e.getMessage(), e);

	ResponseMap responseMap = new ResponseMap();
	responseMap.put(ResponseMap.STATUS_KEY, ResponseMap.STATUS_ERROR);
	responseMap.put(REASONS, e.getMessage());
	return responseMap;
    }

}
