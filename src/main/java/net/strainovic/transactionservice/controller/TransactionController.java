package net.strainovic.transactionservice.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.strainovic.transactionservice.model.ResponseMap;
import net.strainovic.transactionservice.model.Transaction;
import net.strainovic.transactionservice.model.converter.TransactionConverter;
import net.strainovic.transactionservice.model.dto.DtoTransaction;
import net.strainovic.transactionservice.service.TransactionService;

@RestController
@RequestMapping("/transactionservice")
public class TransactionController {

    private static final Log LOGGER = LogFactory.getLog(TransactionController.class);
    private static final String SUM_KEY = "sum";

    @Autowired
    private TransactionService transactionService;

    /**
     *
     * @param transaction_id
     *            URL parameter with ID of transaction
     * @param dtoTransaction
     *            body: { "amount":double,"type":string,"parent_id":long }
     *            parent_id can be null
     * @return Example: if everything OK { "status": "ok" } otherwise:
     *         {"status":"error","reasons":
     *         "Transaction with ID: 1 already exists"}
     */
    @RequestMapping(value = "/transaction/{transaction_id}", method = RequestMethod.PUT)
    public ResponseMap createTransaction(@PathVariable("transaction_id") Long transaction_id,
	    @Valid @RequestBody DtoTransaction dtoTransaction) {
	String type = dtoTransaction.getType();
	Double amount = dtoTransaction.getAmount();
	Long parentId = dtoTransaction.getParent_id();

	Transaction transaction = getTransactionService().createTransaction(transaction_id, type, amount, parentId);

	if (transaction == null) {
	    String errorMessage = String.format(
		    "There are unexpected problem creating transaction ID: %s, type: %s, amount: %s, parentId: %s",
		    transaction_id, type, amount, parentId);
	    LOGGER.error(errorMessage);
	    throw new RuntimeException(errorMessage);
	}

	return new ResponseMap();
    }

    /**
     * Find Transaction ids for given transaction type
     *
     * @param type
     *            type of transaction
     * @return List of transaction ids with specified type
     */
    @RequestMapping(value = "/types/{type}", method = RequestMethod.GET)
    public List<Long> findTransactionIdsByType(@PathVariable("type") String type) {
	return getTransactionService().findIdsByType(type);
    }

    /**
     *
     * Getting a sum of all transactions that are transitively linked by their
     * parent_id to {transation_id}
     *
     * @param transaction_id
     *            id of transaction
     * @return {"sum":15000}
     */
    @RequestMapping(value = "/sum/{transaction_id}", method = RequestMethod.GET)
    public Map<String, Double> getSum(@PathVariable("transaction_id") Long transaction_id) {
	Double sum = getTransactionService().calculateTransactionSum(transaction_id);

	if (sum == null || sum < 0) {
	    String errorMessage = String.format("Sum: %s of transaction with ID: %s is not valid", sum, transaction_id);
	    LOGGER.debug(errorMessage);
	    throw new RuntimeException(errorMessage);
	}

	Map<String, Double> responseMap = new HashMap<>();
	responseMap.put(SUM_KEY, sum);

	return responseMap;
    }

    /**
     *
     * @param transaction_id
     *            id of transaction
     * @return transaction for given ID, it transaction doesn't exists return
     *         error with appropriate message
     */
    @RequestMapping(value = "/transaction/{transaction_id}", method = RequestMethod.GET)
    public DtoTransaction getTransaction(@PathVariable("transaction_id") Long transaction_id) {
	Transaction transaction = getTransactionService().findById(transaction_id);

	if (transaction == null) {
	    String errorMessage = String.format("Transaction with ID: %s doesn't exists", transaction_id);
	    LOGGER.debug(errorMessage);
	    throw new RuntimeException(errorMessage);
	}

	return TransactionConverter.fromInternalToDto(transaction);
    }

    private TransactionService getTransactionService() {
	return transactionService;
    }

}
