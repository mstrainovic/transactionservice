package net.strainovic.transactionservice.dao;

import java.util.List;

import net.strainovic.transactionservice.model.Transaction;

public interface TransactionDao {

    Transaction findById(Long transactionId);

    List<Long> findIdsByType(String type);

    Transaction saveTransaction(Transaction transaction);

}
