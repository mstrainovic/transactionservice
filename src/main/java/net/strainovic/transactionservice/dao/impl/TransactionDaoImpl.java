package net.strainovic.transactionservice.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import net.strainovic.transactionservice.dao.TransactionDao;
import net.strainovic.transactionservice.model.Transaction;

/**
 * Dummy dao in real case this can be call to DB
 */
@Repository
public class TransactionDaoImpl implements TransactionDao {

    private static List<Transaction> TRANSACTIONS = new ArrayList<>();

    @Override
    public Transaction findById(Long transactionId) {
	return TRANSACTIONS.stream().filter(transaction -> transaction.getTransactionId().equals(transactionId))
		.findFirst().orElse(null);
    }

    @Override
    public List<Long> findIdsByType(String type) {
	return TRANSACTIONS.stream().filter(transaction -> transaction.getType().equals(type))
		.map(Transaction::getTransactionId).collect(Collectors.toList());

    }

    @Override
    public Transaction saveTransaction(Transaction transaction) {
	TRANSACTIONS.add(transaction);
	return transaction;
    }
}
