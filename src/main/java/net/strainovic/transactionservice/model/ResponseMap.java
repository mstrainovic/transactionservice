package net.strainovic.transactionservice.model;

import java.util.HashMap;

public class ResponseMap extends HashMap<String, Object> {

    public static final String STATUS_KEY = "status";
    public static final String STATUS_ERROR = "error";
    public static final String STATUS_OK = "ok";

    public ResponseMap() {
	this.put(STATUS_KEY, STATUS_OK);
    }
}
