package net.strainovic.transactionservice.model;

/**
 * Representing one transaction
 */
public class Transaction {

    private Double amount;
    private Transaction parent;
    private Long transactionId;
    private String type;

    public Double getAmount() {
	return amount;
    }

    public Transaction getParent() {
        return parent;
    }

    public Long getTransactionId() {
	return transactionId;
    }

    public String getType() {
	return type;
    }

    public void setAmount(Double amount) {
	this.amount = amount;
    }

    public void setParent(Transaction parent) {
        this.parent = parent;
    }


    public void setTransactionId(Long transactionId) {
	this.transactionId = transactionId;
    }

    public void setType(String type) {
	this.type = type;
    }
}
