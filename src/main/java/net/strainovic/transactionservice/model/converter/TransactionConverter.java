package net.strainovic.transactionservice.model.converter;

import net.strainovic.transactionservice.model.Transaction;
import net.strainovic.transactionservice.model.dto.DtoTransaction;

/**
 * Converting internal Transaction object to DTP representation to be sent to
 * client
 */
public class TransactionConverter {

    public static DtoTransaction fromInternalToDto(Transaction transaction) {
	if (transaction == null) {
	    return null;
	}

	DtoTransaction dtoTransaction = new DtoTransaction();
	dtoTransaction.setAmount(transaction.getAmount());
	dtoTransaction.setType(transaction.getType());

	Transaction parentTransaction = transaction.getParent();
	if (parentTransaction != null) {
	    dtoTransaction.setParent_id(parentTransaction.getTransactionId());
	}

	return dtoTransaction;
    }
}
