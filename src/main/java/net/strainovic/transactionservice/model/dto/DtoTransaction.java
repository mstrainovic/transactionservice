package net.strainovic.transactionservice.model.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class DtoTransaction {

    @Min(0)
    @NotNull
    private Double amount;

    private Long parent_id;

    @NotNull
    private String type;

    public Double getAmount() {
	return amount;
    }

    public Long getParent_id() {
	return parent_id;
    }

    public String getType() {
	return type;
    }

    public void setAmount(Double amount) {
	this.amount = amount;
    }

    public void setParent_id(Long parent_id) {
	this.parent_id = parent_id;
    }

    public void setType(String type) {
	this.type = type;
    }
}
