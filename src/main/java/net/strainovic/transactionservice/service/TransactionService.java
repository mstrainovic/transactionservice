package net.strainovic.transactionservice.service;

import net.strainovic.transactionservice.model.Transaction;

import java.util.List;

public interface TransactionService {

    Double calculateTransactionSum(Long transactionId);

    Transaction createTransaction(Long transactionId, String type, Double amount, Long parentId);

    Transaction findById(Long transactionId);

    List<Long> findIdsByType(String type);

}
