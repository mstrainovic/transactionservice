package net.strainovic.transactionservice.service.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.strainovic.transactionservice.dao.TransactionDao;
import net.strainovic.transactionservice.model.Transaction;
import net.strainovic.transactionservice.service.TransactionService;

@Service("transactionService")
public class TransactionServiceImpl implements TransactionService {

    private static final Log LOGGER = LogFactory.getLog(TransactionServiceImpl.class);

    @Autowired
    private TransactionDao transactionDao;

    /**
     * @param transactionId
     *            id of transaction
     * @return sum of all transactions that are transitively linked by their
     *         parent_id to {transation_id}
     */
    @Override
    public Double calculateTransactionSum(Long transactionId) {
	Transaction transaction = getTransactionDao().findById(transactionId);

	if (transaction == null) {
	    String errorMessage = String.format("Transaction with ID: %s doesn't exists", transactionId);
	    LOGGER.debug(errorMessage);
	    throw new RuntimeException(errorMessage);
	}

	return getSumWithParent(transaction);
    }

    /**
     * Create transaction based on input parameter
     *
     * @param transactionId
     *            if transaction id already exists throw exception
     * @param type
     *            type of transaction
     * @param amount
     *            amount of transaction
     * @param parentId
     *            parent id can be null, cannot be the same as transaction id
     *            and need to exists
     * @return newly created transaction
     */
    @Override
    public Transaction createTransaction(Long transactionId, String type, Double amount, Long parentId) {
	Transaction existingTransaction = getTransactionDao().findById(transactionId);

	if (existingTransaction != null) {
	    String errorMessage = String.format("Transaction with ID: %s already exists", transactionId);
	    LOGGER.error(errorMessage);
	    throw new RuntimeException(errorMessage);
	}

	Transaction transaction = new Transaction();
	transaction.setTransactionId(transactionId);
	transaction.setType(type);
	transaction.setAmount(amount);

	if (parentId != null) {
	    if (parentId.equals(transactionId)) {
		String errorMessage = String.format("Parent ID: %s cannot be the same as Transaction ID: %s", parentId,
			transactionId);
		LOGGER.error(errorMessage);
		throw new RuntimeException(errorMessage);
	    }

	    Transaction parentTransaction = getTransactionDao().findById(parentId);

	    if (parentTransaction == null) {
		String errorMessage = String.format("Parent with ID: %s doesn't exists", parentId);
		LOGGER.error(errorMessage);
		throw new RuntimeException(errorMessage);
	    }

	    transaction.setParent(parentTransaction);
	}

	getTransactionDao().saveTransaction(transaction);

	return transaction;
    }

    /**
     * @param transactionId
     *            id of searched transaction
     * @return Transaction if exists otherwise null
     */
    @Override
    public Transaction findById(Long transactionId) {
	return getTransactionDao().findById(transactionId);
    }

    /**
     * @param type
     *            of transaction
     * @return list of ids for transactions with given tipe
     */
    @Override
    public List<Long> findIdsByType(String type) {
	return getTransactionDao().findIdsByType(type);
    }

    private Double getSumWithParent(Transaction transaction) {
	Double sum = 0d;

	sum += transaction.getAmount();

	Transaction parent = transaction.getParent();
	if (parent != null) {
	    sum += getSumWithParent(parent);
	}

	return sum;

    }

    private TransactionDao getTransactionDao() {
	return transactionDao;
    }
}
