package net.strainovic.transactionservice.controller;

import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.jsonpath.JsonPath;

import net.minidev.json.JSONArray;
import net.strainovic.transactionservice.model.ResponseMap;
import net.strainovic.transactionservice.model.Transaction;
import net.strainovic.transactionservice.model.dto.DtoTransaction;
import net.strainovic.transactionservice.service.TransactionService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TransactionControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private TransactionService transactionService;

    /**
     * Creating transaction with all positive data without parent ID
     */
    @Test
    public void createTransactionBasicPositiveTest() {
	Transaction mockedTransaction = new Transaction();

	when(getTransactionService().createTransaction(anyLong(), anyString(), anyDouble(), anyLong()))
		.thenReturn(mockedTransaction);

	DtoTransaction dtoTransaction = new DtoTransaction();
	dtoTransaction.setAmount(999.99);
	dtoTransaction.setType("shopping");

	ResponseEntity<String> response = getRestTemplate().exchange("/transactionservice/transaction/{transaction_id}",
		HttpMethod.PUT, new HttpEntity<>(dtoTransaction), String.class, 1L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray success = JsonPath.read(json, "$..status");
	Assert.assertEquals(ResponseMap.STATUS_OK, success.get(0));
    }

    /**
     * Creating transaction with error on service side. Get message and send in
     * response example: {"status":"error","reasons":
     * "Transaction with ID: 1 already exists"}
     */
    @Test
    public void createTransactionExpectedServiceErrorTest() {
	when(getTransactionService().createTransaction(anyLong(), anyString(), anyDouble(), anyLong()))
		.thenThrow(new RuntimeException("Transaction with ID: 1 already exists"));

	DtoTransaction dtoTransaction = new DtoTransaction();
	dtoTransaction.setType("shopping");
	dtoTransaction.setAmount(99.99);

	ResponseEntity<String> response = getRestTemplate().exchange("/transactionservice/transaction/{transaction_id}",
		HttpMethod.PUT, new HttpEntity<>(dtoTransaction), String.class, 1L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray success = JsonPath.read(json, "$..status");
	Assert.assertEquals(ResponseMap.STATUS_ERROR, success.get(0));

	JSONArray message = JsonPath.read(json, "$..reasons");
	Assert.assertEquals("Transaction with ID: 1 already exists", message.get(0));
    }

    @Test
    public void createTransactionUnexpectedServiceErrorTest() {
	when(getTransactionService().createTransaction(anyLong(), anyString(), anyDouble(), anyLong()))
		.thenReturn(null);

	DtoTransaction dtoTransaction = new DtoTransaction();
	dtoTransaction.setType("shopping");
	dtoTransaction.setAmount(99.99);

	ResponseEntity<String> response = getRestTemplate().exchange("/transactionservice/transaction/{transaction_id}",
		HttpMethod.PUT, new HttpEntity<>(dtoTransaction), String.class, 1L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray success = JsonPath.read(json, "$..status");
	Assert.assertEquals(ResponseMap.STATUS_ERROR, success.get(0));

	JSONArray message = JsonPath.read(json, "$..reasons");
	Assert.assertEquals(
		"There are unexpected problem creating transaction ID: 1, type: shopping, amount: 99.99, parentId: null",
		message.get(0));
    }

    /**
     * Creating transaction with all positive data and parent ID
     */
    @Test
    public void createTransactionWithParentPositiveTest() {
	Transaction mockedTransaction = new Transaction();

	when(getTransactionService().createTransaction(anyLong(), anyString(), anyDouble(), anyLong()))
		.thenReturn(mockedTransaction);

	DtoTransaction dtoTransaction = new DtoTransaction();
	dtoTransaction.setAmount(999.99);
	dtoTransaction.setType("shopping");
	dtoTransaction.setParent_id(2L);

	ResponseEntity<String> response = getRestTemplate().exchange("/transactionservice/transaction/{transaction_id}",
		HttpMethod.PUT, new HttpEntity<>(dtoTransaction), String.class, 1L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray success = JsonPath.read(json, "$..status");
	Assert.assertEquals(ResponseMap.STATUS_OK, success.get(0));
    }

    /**
     * Find transactions ids by type return two element
     */
    @Test
    public void findTransactionIdsByTypeBasicPositiveTest() {
	when(getTransactionService().findIdsByType("shopping")).thenReturn(Arrays.asList(1L, 2L));

	ResponseEntity<String> response = getRestTemplate().getForEntity("/transactionservice/types/{type}",
		String.class, "shopping");

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray responseMap = JsonPath.read(json, "$");
	Assert.assertEquals(1, responseMap.get(0));
	Assert.assertEquals(2, responseMap.get(1));
    }

    /**
     * Find transaction ids by type that doesn't exists so return empty set
     */
    @Test
    public void findTransactionIdsByTypeEmptyResultSetTest() {
	when(getTransactionService().findIdsByType("cars")).thenReturn(new ArrayList<>());

	ResponseEntity<String> response = getRestTemplate().getForEntity("/transactionservice/types/{type}",
		String.class, "shopping");

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray responseMap = JsonPath.read(json, "$");
	Assert.assertEquals(0, responseMap.size());

    }

    /**
     * Find transaction by id that doesn't exists. Return error with message
     */
    @Test
    public void getTransactionByIdDoesntExistsTest() {
	ResponseEntity<String> response = getRestTemplate()
		.getForEntity("/transactionservice/transaction/{transaction_id}", String.class, 10L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray success = JsonPath.read(json, "$..status");
	Assert.assertEquals(ResponseMap.STATUS_ERROR, success.get(0));

	JSONArray message = JsonPath.read(json, "$..reasons");
	Assert.assertEquals("Transaction with ID: 10 doesn't exists", message.get(0));

    }

    /**
     * Find transaction by id that exists. Return DtoTransaction object
     */
    @Test
    public void getTransactionByIdPositiveTest() {
	Transaction mockedTransaction = new Transaction();
	mockedTransaction.setTransactionId(1L);
	mockedTransaction.setType("shopping");
	mockedTransaction.setAmount(100.00);

	when(getTransactionService().findById(1L)).thenReturn(mockedTransaction);

	ResponseEntity<String> response = getRestTemplate()
		.getForEntity("/transactionservice/transaction/{transaction_id}", String.class, 1L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	LinkedHashMap<String, Object> responceMap = JsonPath.read(json, "$");
	Assert.assertEquals(100.00, responceMap.get("amount"));
	Assert.assertNull(responceMap.get("parent_id"));
	Assert.assertEquals("shopping", responceMap.get("type"));
    }

    /**
     * Testing sum when service throw exception
     */
    @Test
    public void sumTransactionErrorTest() {
	when(getTransactionService().calculateTransactionSum(10L))
		.thenThrow(new RuntimeException("Transaction with ID: 10 doesn't exists"));

	ResponseEntity<String> response = getRestTemplate().getForEntity("/transactionservice/sum/{transaction_id}",
		String.class, 10L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray success = JsonPath.read(json, "$..status");
	Assert.assertEquals(ResponseMap.STATUS_ERROR, success.get(0));

	JSONArray message = JsonPath.read(json, "$..reasons");
	Assert.assertEquals("Transaction with ID: 10 doesn't exists", message.get(0));
    }

    /**
     * Sum transaction positive test
     */
    @Test
    public void sumTransactionPositiveTest() {
	when(getTransactionService().calculateTransactionSum(1L)).thenReturn(100.00);

	ResponseEntity<String> response = getRestTemplate().getForEntity("/transactionservice/sum/{transaction_id}",
		String.class, 1L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray sum = JsonPath.read(json, "$..sum");
	Assert.assertEquals(100.00, sum.get(0));
    }

    /**
     * Testing sum when service return negative or null result
     */
    @Test
    public void sumTransactionUnexpectedErrorTest() {
	when(getTransactionService().calculateTransactionSum(10L)).thenReturn(null);

	ResponseEntity<String> response = getRestTemplate().getForEntity("/transactionservice/sum/{transaction_id}",
		String.class, 10L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray success = JsonPath.read(json, "$..status");
	Assert.assertEquals(ResponseMap.STATUS_ERROR, success.get(0));

	JSONArray message = JsonPath.read(json, "$..reasons");
	Assert.assertEquals("Sum: null of transaction with ID: 10 is not valid", message.get(0));
    }

    /**
     * Validating amount must be positive number
     */
    @Test
    public void validateDtoAmountNegativeTest() {
	DtoTransaction dtoTransaction = new DtoTransaction();
	dtoTransaction.setAmount(-1.99);
	dtoTransaction.setType("shopping");

	ResponseEntity<String> response = getRestTemplate().exchange("/transactionservice/transaction/{transaction_id}",
		HttpMethod.PUT, new HttpEntity<>(dtoTransaction), String.class, 1L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray success = JsonPath.read(json, "$..status");
	Assert.assertEquals(ResponseMap.STATUS_ERROR, success.get(0));

	JSONArray message = JsonPath.read(json, "$..reasons");
	Assert.assertTrue(message.get(0).toString().startsWith("Validation failed for argument"));
	Assert.assertTrue(message.get(0).toString()
		.contains("[Field error in object 'dtoTransaction' on field 'amount': rejected value [-1.99]"));
    }

    /**
     * Validating amount cannot be null
     */
    @Test
    public void validateDtoAmountNullTest() {
	DtoTransaction dtoTransaction = new DtoTransaction();
	dtoTransaction.setAmount(null);
	dtoTransaction.setType("shopping");

	ResponseEntity<String> response = getRestTemplate().exchange("/transactionservice/transaction/{transaction_id}",
		HttpMethod.PUT, new HttpEntity<>(dtoTransaction), String.class, 1L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray success = JsonPath.read(json, "$..status");
	Assert.assertEquals(ResponseMap.STATUS_ERROR, success.get(0));

	JSONArray message = JsonPath.read(json, "$..reasons");
	Assert.assertTrue(message.get(0).toString().startsWith("Validation failed for argument"));
	Assert.assertTrue(message.get(0).toString()
		.contains("[Field error in object 'dtoTransaction' on field 'amount': rejected value [null]"));
    }

    /**
     * Validating amount cannot be null
     */
    @Test
    public void validateDtoTypeNullTest() {
	DtoTransaction dtoTransaction = new DtoTransaction();
	dtoTransaction.setAmount(1.99);
	dtoTransaction.setType(null);

	ResponseEntity<String> response = getRestTemplate().exchange("/transactionservice/transaction/{transaction_id}",
		HttpMethod.PUT, new HttpEntity<>(dtoTransaction), String.class, 1L);

	Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	String json = response.getBody();

	JSONArray success = JsonPath.read(json, "$..status");
	Assert.assertEquals(ResponseMap.STATUS_ERROR, success.get(0));

	JSONArray message = JsonPath.read(json, "$..reasons");
	Assert.assertTrue(message.get(0).toString().startsWith("Validation failed for argument"));
	Assert.assertTrue(message.get(0).toString()
		.contains("[Field error in object 'dtoTransaction' on field 'type': rejected value [null]"));
    }

    private TestRestTemplate getRestTemplate() {
	return restTemplate;
    }

    private TransactionService getTransactionService() {
	return transactionService;
    }

}