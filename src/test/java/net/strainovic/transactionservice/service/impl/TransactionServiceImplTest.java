package net.strainovic.transactionservice.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import net.strainovic.transactionservice.dao.TransactionDao;
import net.strainovic.transactionservice.model.Transaction;
import net.strainovic.transactionservice.service.TransactionService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionServiceImplTest {

    @MockBean
    private TransactionDao _transactionDao;

    /**
     * Mocked transactions used for testing
     */
    private List<Transaction> mockedTransactions = new ArrayList<>();

    @Autowired
    private TransactionService transactionService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * Creating transaction with ID that already exists. Since not specified in
     * assignment return status: "error" with reason
     */
    @Test
    public void createTransactionAlreadyExistsTest() {
	thrown.expect(RuntimeException.class);
	thrown.expectMessage("Transaction with ID: 1 already exists");

	Transaction mockedTransaction = getMockedTransaction(1L);

	when(getTransactionDao().findById(1L)).thenReturn(mockedTransaction);

	getTransactionService().createTransaction(1L, "shopping", 99.99, null);

	verify(getTransactionDao(), never()).saveTransaction(any(Transaction.class));
    }

    /**
     * Creating transaction with all positive data without parent ID
     */
    @Test
    public void createTransactionBasicPositiveTest() {
	when(getTransactionDao().findById(1L)).thenReturn(null);

	Transaction transaction = getTransactionService().createTransaction(1L, "shopping", 99.99, null);

	verify(getTransactionDao(), times(1)).saveTransaction(transaction);

	Assert.assertNotNull(transaction);
	Assert.assertEquals(Long.valueOf(1), transaction.getTransactionId());
	Assert.assertEquals("shopping", transaction.getType());
	Assert.assertEquals(Double.valueOf(99.99), transaction.getAmount());
	Assert.assertNull(transaction.getParent());
    }

    /**
     * Creating transaction with parent ID the same as transaction ID. It's not
     * valid to have the same parent_id and transaction_id
     */
    @Test
    public void createTransactionParentIdTheSameAsIdTest() {
	thrown.expect(RuntimeException.class);
	thrown.expectMessage("Parent ID: 1 cannot be the same as Transaction ID: 1");

	when(getTransactionDao().findById(1L)).thenReturn(null);

	getTransactionService().createTransaction(1L, "shopping", 99.99, 1L);

	verify(getTransactionDao(), never()).saveTransaction(any(Transaction.class));
    }

    /**
     * Creating transaction with parent ID that doesn't exists
     */
    @Test
    public void createTransactionWithParentIdDoesntExistsTest() {
	thrown.expect(RuntimeException.class);
	thrown.expectMessage("Parent with ID: 2 doesn't exists");

	when(getTransactionDao().findById(1L)).thenReturn(null);

	getTransactionService().createTransaction(1L, "shopping", 99.99, 2L);

	verify(getTransactionDao(), never()).saveTransaction(any(Transaction.class));

    }

    /**
     * Setup some mocked transactions for testing
     */
    @Before
    public void setupMockedTransactions() {
	Transaction mockedTransaction1 = new Transaction();
	mockedTransaction1.setTransactionId(1L);
	mockedTransaction1.setType("shopping");
	mockedTransaction1.setAmount(100.00);

	Transaction mockedTransaction2 = new Transaction();
	mockedTransaction2.setTransactionId(2L);
	mockedTransaction2.setType("shopping");
	mockedTransaction2.setAmount(50.00);
	mockedTransaction2.setParent(mockedTransaction1);

	Transaction mockedTransaction3 = new Transaction();
	mockedTransaction3.setTransactionId(3L);
	mockedTransaction3.setType("shopping");
	mockedTransaction3.setAmount(30.00);
	mockedTransaction3.setParent(mockedTransaction2);

	Transaction mockedTransaction4 = new Transaction();
	mockedTransaction4.setTransactionId(4L);
	mockedTransaction4.setType("cars");
	mockedTransaction4.setAmount(10000.00);

	getMockedTransactions().add(mockedTransaction1);
	getMockedTransactions().add(mockedTransaction2);
	getMockedTransactions().add(mockedTransaction3);
	getMockedTransactions().add(mockedTransaction4);
    }

    /**
     * Testing sum without parent ID, expected to return only sum of Transaction
     * with ID: 1 - 100.00
     */
    @Test
    public void sumTransactionWithIdDoesntExistsTest() {
	thrown.expect(RuntimeException.class);
	thrown.expectMessage("Transaction with ID: 1 doesn't exists");

	when(getTransactionService().findById(1L)).thenReturn(null);

	getTransactionService().calculateTransactionSum(1L);
    }

    /**
     * Testing sum with only one parent ID in deep, expected to return sum of
     * Transaction with ID: 1 - 100.00 and ID:2 - 50 - SUM: 150
     */
    @Test
    public void sumWithOneParentTransitivelyTest() {
	Transaction mockedTransaction = getMockedTransaction(2L);
	when(getTransactionService().findById(2L)).thenReturn(mockedTransaction);

	Double sum = getTransactionService().calculateTransactionSum(2L);

	Assert.assertEquals(Double.valueOf(150), sum);

    }

    /**
     * Testing sum with only one parent ID in deep, expected to return sum of
     * Transaction with ID: 1 - 100.00, ID: 2 - 50, ID: 3 - 30 - SUM: 180
     */
    @Test
    public void sumWithTwoParentTransitivelyTest() {
	Transaction mockedTransaction = getMockedTransaction(3L);
	when(getTransactionService().findById(3L)).thenReturn(mockedTransaction);

	Double sum = getTransactionService().calculateTransactionSum(3L);

	Assert.assertEquals(Double.valueOf(180), sum);
    }

    /**
     * Testing sum without parent ID, expected to return only sum of Transaction
     * with ID: 1 - 100.00
     */
    @Test
    public void sumWithoutParentsTest() {
	Transaction mockedTransaction = getMockedTransaction(1L);
	when(getTransactionService().findById(1L)).thenReturn(mockedTransaction);

	Double sum = getTransactionService().calculateTransactionSum(1L);

	Assert.assertEquals(Double.valueOf(100), sum);
    }

    private Transaction getMockedTransaction(Long transactionId) {
	return getMockedTransactions().stream()
		.filter(transaction -> transaction.getTransactionId().equals(transactionId)).findFirst()
		.orElseThrow(() -> new RuntimeException(
			String.format("Mocked transaction with ID: %s doesn't exists", transactionId)));
    }

    private List<Transaction> getMockedTransactions() {
	return mockedTransactions;
    }

    private TransactionDao getTransactionDao() {
	return _transactionDao;
    }

    private TransactionService getTransactionService() {
	return transactionService;
    }

}